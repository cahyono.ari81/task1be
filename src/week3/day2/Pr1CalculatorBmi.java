package week3.day2;

import java.util.Scanner;

public class Pr1CalculatorBmi {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        boolean flag = false;

        while (!flag) {
            try {
                System.out.println("Kalkulator BMI");
                System.out.print("Berat Badan : ");
                double bb = scan.nextDouble();
                System.out.print("Tinggi Badan : ");
                double tb = scan.nextDouble();
                countBmi(bb, tb);
                flag = true;
            } catch (Exception var7) {
                scan.nextLine();
                System.out.println("Masukkan input yang sesuai");
            }
        }

        scan.close();
    }

    private static void countBmi(double bb, double tb) {
        double rumusBmi = bb / Math.pow(tb / 100.0D, 2.0D);
        if (rumusBmi > 30.0D) {
            System.out.println("Anda Obesitas");
        } else if (rumusBmi > 25.0D) {
            System.out.println("Anda Kelebihan Berat Badan");
        } else if (rumusBmi > 18.5D) {
            System.out.println("Anda Sehat");
        } else {
            System.out.println("Anda Kekurangan Berat Badan");
        }
    }
}
