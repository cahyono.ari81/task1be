package com.example.day3OneToOne.service.impl;

import com.example.day3OneToOne.dto.MobilDetailDto;
import com.example.day3OneToOne.dto.MobilDto;
import com.example.day3OneToOne.model.Mobil;
import com.example.day3OneToOne.model.MobilDetail;
import com.example.day3OneToOne.repository.MobilRepository;
import com.example.day3OneToOne.service.MobilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MobilServiceImpl implements MobilService {

    @Autowired
    private MobilRepository mobilRepository;

    @Override
    public void create(MobilDto mobilDto) {
        Mobil mobil = converToMobil(mobilDto);
        this.mobilRepository.save(mobil);
    }

    @Override
    public List<MobilDto> all() {
        List<Mobil> mobils = this.mobilRepository.findByIsDeleted(false);
        List<MobilDto> mobilDtos = new ArrayList<>();
        for (Mobil mobil : mobils){
            mobilDtos.add(converToMobilDto(mobil));
        }
        return mobilDtos;
    }

    @Override
    public MobilDto getByIdAndIsDeleted(Long id) {
        Optional<Mobil> mobil = Optional.ofNullable(this.mobilRepository.findByIdAndIsDeleted(id));
        return mobil.map(this::converToMobilDto).orElse(null);
    }

    @Override
    public MobilDto update(MobilDto mobilDto) {
        Optional<Mobil> optionalMobil = this.mobilRepository.findById(mobilDto.getId());
        if(optionalMobil.isPresent()){
            MobilDetail mobilDetail = new MobilDetail();

            mobilDetail.setColor(mobilDto.getMobilDetailDto().getColor());
            mobilDetail.setYear(mobilDto.getMobilDetailDto().getYear());
            mobilDetail.setPrice(mobilDto.getMobilDetailDto().getPrice());
            mobilDetail.setIsNew(mobilDto.getMobilDetailDto().getIsNew());

            Mobil mobil = optionalMobil.get();
            mobil.setId(mobilDto.getId());
            mobil.setBrand(mobilDto.getBrand());
            mobil.setIsDeleted(mobilDto.getIsDeleted());
            mobil.setMobilDetail(mobilDetail);

            this.mobilRepository.save(mobil);
            return converToMobilDto(mobil);
        }
        return null;
    }

    @Override
    public MobilDto delete(Long id) {
        Optional<Mobil> optionalMobil = this.mobilRepository.findById(id);
        if(optionalMobil.isPresent()){
           Mobil mobil = optionalMobil.get();
           mobil.setIsDeleted(true);
           this.mobilRepository.save(mobil);
           return converToMobilDto(mobil);
        }
        return null;
    }

    private Mobil converToMobil(MobilDto mobilDto){

        MobilDetail mobilDetail = new MobilDetail();
        mobilDetail.setColor(mobilDto.getMobilDetailDto().getColor());
        mobilDetail.setYear(mobilDto.getMobilDetailDto().getYear());
        mobilDetail.setPrice(mobilDto.getMobilDetailDto().getPrice());
        mobilDetail.setIsNew(mobilDto.getMobilDetailDto().getIsNew());

        Mobil mobil = new Mobil();
        mobil.setBrand(mobilDto.getBrand());
        mobil.setIsDeleted(mobilDto.getIsDeleted() != null ? mobilDto.getIsDeleted() : false);
        mobil.setMobilDetail(mobilDetail);

        return mobil;
    }

    private MobilDto converToMobilDto(Mobil mobil){

        MobilDetailDto mobilDetailDto = new MobilDetailDto();
        mobilDetailDto.setColor(mobil.getMobilDetail().getColor());
        mobilDetailDto.setPrice(mobil.getMobilDetail().getPrice());
        mobilDetailDto.setYear(mobil.getMobilDetail().getYear());
        mobilDetailDto.setIsNew(mobil.getMobilDetail().getIsNew());

        MobilDto mobilDto = new MobilDto();
        mobilDto.setId(mobil.getId());
        mobilDto.setIsDeleted(mobil.getIsDeleted());
        mobilDto.setBrand(mobil.getBrand());
        mobilDto.setMobilDetailDto(mobilDetailDto);

        return mobilDto;

    }
}
