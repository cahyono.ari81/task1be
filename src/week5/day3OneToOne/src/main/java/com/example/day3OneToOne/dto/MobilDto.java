package com.example.day3OneToOne.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;

@NoArgsConstructor
@Setter
@Getter
public class MobilDto {

    private Long id;

    private Boolean isDeleted;
    private String brand;

    private MobilDetailDto mobilDetailDto;
}
