package com.example.day3OneToOne;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Day3OneToOneApplication {

	public static void main(String[] args) {
		SpringApplication.run(Day3OneToOneApplication.class, args);
	}

}
