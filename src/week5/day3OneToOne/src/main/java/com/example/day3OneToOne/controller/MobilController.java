package com.example.day3OneToOne.controller;

import com.example.day3OneToOne.dto.MobilDto;
import com.example.day3OneToOne.service.MobilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/mobil")
public class MobilController {

    @Autowired
    private MobilService mobilService;

    // http://localhost:8084/api/mobil
    @GetMapping
    public String test(){
        return "Welcome In Login Api";
    }

    // http://localhost:8084/api/mobil/create
    @PostMapping("/create")
    public String create(@RequestBody MobilDto mobilDto){
        this.mobilService.create(mobilDto);
        return "create mobil data success";
    }

    // http://localhost:8084/api/mobil/all
    @GetMapping("/all")
    public ResponseEntity<List<MobilDto>> all(){
        return new ResponseEntity<>(this.mobilService.all(), HttpStatus.OK);
    }
    // http://localhost:8084/api/mobil/1
    @GetMapping("/{id}")
    public ResponseEntity<MobilDto> getByIdAndIsDeleted(@PathVariable Long id){
        return new ResponseEntity<>(this.mobilService.getByIdAndIsDeleted(id), HttpStatus.OK);
    }

    // http://localhost:8084/api/mobil/
    @PutMapping("/")
    public MobilDto update(@RequestBody MobilDto mobilDto){
        return this.mobilService.update(mobilDto);
    }

    // http://localhost:8084/api/mobil/all
    @DeleteMapping("/{id}")
    public MobilDto delete(@PathVariable Long id){
        return this.mobilService.delete(id);
    }
}
