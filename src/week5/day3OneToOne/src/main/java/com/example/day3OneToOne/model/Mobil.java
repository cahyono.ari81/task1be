package com.example.day3OneToOne.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;

@Entity
@Table(name = "mobil")
@NoArgsConstructor
@Setter
@Getter
public class Mobil {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Boolean isDeleted;
    private String brand;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "mobil_detail_id", referencedColumnName = "id")
    private MobilDetail mobilDetail;

    @Override
    public String toString() {
        return "Mobil{" +
                "id=" + id +
                ", isDeleted=" + isDeleted +
                ", brand='" + brand + '\'' +
                ", mobilDetail=" + mobilDetail +
                '}';
    }
}
