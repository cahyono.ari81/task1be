package com.example.day3OneToOne.repository;

import com.example.day3OneToOne.dto.MobilDto;
import com.example.day3OneToOne.model.Mobil;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
public interface MobilRepository extends JpaRepository<Mobil, Long> {

    public List<Mobil> findByIsDeleted(boolean bool);

    @Query(value = "SELECT * FROM mobil WHERE id = ? AND is_deleted = false", nativeQuery = true)
    public Mobil findByIdAndIsDeleted(Long id);
}
