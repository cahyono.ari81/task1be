package com.example.day3OneToOne.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "mobil_detail")
@NoArgsConstructor
@Setter
@Getter
public class MobilDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String color;
    private Boolean isNew;
    private Integer year;
    private Double price;

}
