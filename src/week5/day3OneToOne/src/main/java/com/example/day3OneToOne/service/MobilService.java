package com.example.day3OneToOne.service;

import com.example.day3OneToOne.dto.MobilDto;
import org.springframework.stereotype.Service;

import java.util.List;

public interface MobilService {

    public void create(MobilDto mobilDto);

    public List<MobilDto> all();

    public MobilDto getByIdAndIsDeleted(Long id);

    public MobilDto update(MobilDto mobilDto);

    public MobilDto delete(Long id);
}
