package com.example.day3OneToOne.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Setter
@Getter
public class MobilDetailDto {

    private Long id;
    private String color;
    private Boolean isNew;
    private Integer year;
    private Double price;

    @Override
    public String toString() {
        return "MobilDetailDto{" +
                "id=" + id +
                ", color='" + color + '\'' +
                ", isNew=" + isNew +
                ", year=" + year +
                ", price=" + price +
                '}';
    }
}
