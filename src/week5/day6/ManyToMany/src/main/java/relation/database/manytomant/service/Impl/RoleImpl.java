package relation.database.manytomant.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import relation.database.manytomant.dto.RoleDto;
import relation.database.manytomant.model.Role;
import relation.database.manytomant.repository.RoleRepository;
import relation.database.manytomant.service.RoleService;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoleImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public String generateRole() {
        String[] roleName = {"admin", "customer"};
        for (String name : roleName) {
            Role role = new Role();
            role.setName(name);
            this.roleRepository.save(role);
        }
        return "Role Admin And Customer Success Store In DB";
    }

    @Override
    public List<RoleDto> getAllRole() {
        final List<Role> all = this.roleRepository.findAll();
        List<RoleDto> roleDtoList = new ArrayList<>();
        for (Role data : all){
            RoleDto roleDto = new RoleDto();
            roleDto.setName(data.getName());
            roleDtoList.add(roleDto);
        }
        return roleDtoList;
    }


}
