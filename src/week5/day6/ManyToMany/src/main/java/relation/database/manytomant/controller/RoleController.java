package relation.database.manytomant.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import relation.database.manytomant.dto.RoleDto;
import relation.database.manytomant.service.RoleService;

import java.util.List;

@RestController
@RequestMapping("/api/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping("/generate-role")
    private String generateRole(){
        return this.roleService.generateRole();
    }

    @GetMapping("/")
    private List<RoleDto> getAllRole(){
        return this.roleService.getAllRole();
    }
}
