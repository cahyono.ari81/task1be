package relation.database.manytomant.service;

import relation.database.manytomant.dto.UserDto;

import java.util.List;

public interface UserService {

    String generateUser();
    List<UserDto> getAllUser();
    UserDto getUser(String name);

}
