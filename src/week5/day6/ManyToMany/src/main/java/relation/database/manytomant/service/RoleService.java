package relation.database.manytomant.service;

import relation.database.manytomant.dto.RoleDto;

import java.util.List;

public interface RoleService {

    String generateRole();
    List<RoleDto> getAllRole();
}
