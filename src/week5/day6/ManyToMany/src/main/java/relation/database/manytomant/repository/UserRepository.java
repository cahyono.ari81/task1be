package relation.database.manytomant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import relation.database.manytomant.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    public User findByName(String name);
}
