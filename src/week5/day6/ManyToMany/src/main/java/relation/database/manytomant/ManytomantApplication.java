package relation.database.manytomant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManytomantApplication {

	public static void main(String[] args) {
		SpringApplication.run(ManytomantApplication.class, args);
	}

}
