package relation.database.manytomant.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import relation.database.manytomant.dto.UserDto;
import relation.database.manytomant.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/generate-user")
    private String generateUser() {
        return this.userService.generateUser();
    }

    @GetMapping("/")
    private List<UserDto> getAllUser() {
        return this.userService.getAllUser();
    }

    @GetMapping("/{name}")
    private UserDto getAllUser(@PathVariable String name) {
        return this.userService.getUser(name);
    }
}
