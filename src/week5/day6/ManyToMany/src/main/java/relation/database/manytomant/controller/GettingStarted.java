package relation.database.manytomant.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/test")
public class GettingStarted {

    @GetMapping("/")
    public String welcome(){
        return "Welcome To Task Many to Many";
    }

}
