package relation.database.manytomant.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import relation.database.manytomant.dto.UserDto;
import relation.database.manytomant.model.Role;
import relation.database.manytomant.model.User;
import relation.database.manytomant.repository.RoleRepository;
import relation.database.manytomant.repository.UserRepository;
import relation.database.manytomant.service.UserService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public String generateUser() {

        String[] userCustomer = {randomName("customer"), randomName("customer")};
        String[] userAdmin = {randomName("admin"), randomName("admin")};

        addDataUser("customer", userCustomer);
        addDataUser("admin", userAdmin);

        return "Generate User Success";
    }

    @Override
    public List<UserDto> getAllUser() {
        final List<User> all = this.userRepository.findAll();
        List<UserDto> userDtoList = new ArrayList<>();
        for (User data : all){
            UserDto userDto = new UserDto();
            userDto.setName(data.getName());
            userDtoList.add(userDto);
        }
        return userDtoList;
    }

    @Override
    public UserDto getUser(String name) {
        final User user = this.userRepository.findByName(name);
        UserDto userDto = new UserDto();
        userDto.setName(user.getName());
        userDto.setEmail(user.getEmail());
        return userDto;
    }

    private void addDataUser(String level, String[] nameData){
        for(String name : nameData){
            User user = new User();
            user.setName(name);
            user.setEmail(name.concat("@gmail.com"));
            Role role = this.roleRepository.findByName(level);
            Set<Role> roleSet = new HashSet<>();
            roleSet.add(role);
            user.setRole(roleSet);
            this.userRepository.save(user);
        }
    }

    private String randomName(String name){
        return name.concat(String.valueOf(getRandomNumber(1, 100)));
    }

    public int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }
}
