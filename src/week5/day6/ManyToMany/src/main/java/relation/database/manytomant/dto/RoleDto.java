package relation.database.manytomant.dto;

import lombok.*;

import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class RoleDto {
    private String name;
    private Set<UserDto> customer;
}
