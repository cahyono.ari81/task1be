package com.example.OneToManyRelationDBTask2.dto;

import com.example.OneToManyRelationDBTask2.model.Chapter;
import lombok.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class BookDto {

    private Long id;
    private String title;
    private String author;
    private Timestamp year;
    private Boolean isDeleted;
    private List<Chapter> chapterList;
}
