package com.example.OneToManyRelationDBTask2.controller;

import com.example.OneToManyRelationDBTask2.dto.BookDto;
import com.example.OneToManyRelationDBTask2.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/book")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping("/all")
    private ResponseEntity<List<BookDto>> all(){
        return new ResponseEntity<>(this.bookService.all(), HttpStatus.OK);
    }

    @PostMapping("/create")
    private ResponseEntity<BookDto> create(@RequestBody BookDto bookDto) {
       return new ResponseEntity<>(this.bookService.create(bookDto), HttpStatus.CREATED);
    }
}
