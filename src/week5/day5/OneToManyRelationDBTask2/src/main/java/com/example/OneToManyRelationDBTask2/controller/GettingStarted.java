package com.example.OneToManyRelationDBTask2.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/welcome")
public class GettingStarted {

    @GetMapping
    public ResponseEntity<String> welcome() {
        String welcomeText = "Welcome to Task One To Many Relation";
        return new ResponseEntity<>(welcomeText, HttpStatus.OK);
    }
}
