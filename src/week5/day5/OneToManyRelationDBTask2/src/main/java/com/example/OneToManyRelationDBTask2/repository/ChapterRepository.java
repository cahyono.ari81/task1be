package com.example.OneToManyRelationDBTask2.repository;

import com.example.OneToManyRelationDBTask2.model.Book;
import com.example.OneToManyRelationDBTask2.model.Chapter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChapterRepository extends JpaRepository<Chapter, Long> {
    @Query(value = "SELECT * FROM chapter INNER JOIN book ON book.id = chapter.book_id WHERE chapter.name = ?", nativeQuery = true)
    public List<Chapter> getAllJoinBookByName(String name);
}
