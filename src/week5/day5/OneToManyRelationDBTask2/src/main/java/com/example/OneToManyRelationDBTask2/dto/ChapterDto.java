package com.example.OneToManyRelationDBTask2.dto;

import com.example.OneToManyRelationDBTask2.model.Book;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class ChapterDto {

    private Long id;
    private String name;
    private String content;
    private Boolean isDeleted;
    private Book book;
}
