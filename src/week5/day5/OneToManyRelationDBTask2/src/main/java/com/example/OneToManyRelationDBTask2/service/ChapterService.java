package com.example.OneToManyRelationDBTask2.service;

import com.example.OneToManyRelationDBTask2.dto.ChapterDto;

import java.util.List;

public interface ChapterService {
    public List<ChapterDto> getAllJoinBookByName(String name);
}
