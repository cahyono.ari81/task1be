package com.example.OneToManyRelationDBTask2.repository;

import com.example.OneToManyRelationDBTask2.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    @Query(value = "SELECT * FROM book INNER JOIN chapter ON book.id = chapter.book_id GROUP BY book.id", nativeQuery = true)
    public List<Book> getAllJoinChapter();
}
