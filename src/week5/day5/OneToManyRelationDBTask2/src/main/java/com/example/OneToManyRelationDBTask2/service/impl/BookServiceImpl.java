package com.example.OneToManyRelationDBTask2.service.impl;

import com.example.OneToManyRelationDBTask2.dto.BookDto;
import com.example.OneToManyRelationDBTask2.model.Book;
import com.example.OneToManyRelationDBTask2.model.Chapter;
import com.example.OneToManyRelationDBTask2.repository.BookRepository;
import com.example.OneToManyRelationDBTask2.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Override
    public List<BookDto> all() {
        List<Book> books = this.bookRepository.getAllJoinChapter();
        List<BookDto> bookDtos = new ArrayList<>();
        for (Book book : books){
            bookDtos.add(converToBookDto(book));
        }
        System.out.println(bookDtos);

        return bookDtos;
    }

    @Override
    public BookDto create(BookDto bookDto) {
        Book book = this.bookRepository.save(converToBook(bookDto));
        return converToBookDto(book);
    }

    private Book converToBook(BookDto bookDto){

        Book book = new Book();
        book.setTitle(bookDto.getTitle());
        book.setAuthor(bookDto.getAuthor());
        book.setYear(bookDto.getYear());

        if(bookDto.getChapterList() != null){
            List<Chapter> chapterList = new ArrayList<>();
            for (Chapter chapter : bookDto.getChapterList()) {
                Chapter chapterData = new Chapter();

                chapterData.setId(chapter.getId());
                chapterData.setName(chapter.getName());
                chapterData.setContent(chapter.getContent());
                chapterData.setBook(book);
                chapterList.add(chapterData);
                book.setChapterList(chapterList);
            }
        }

        return book;
    }

    private BookDto converToBookDto(Book book){
        BookDto bookDto = new BookDto();
        bookDto.setId(book.getId());
        bookDto.setTitle(book.getTitle());
        bookDto.setAuthor(book.getAuthor());
        bookDto.setYear(book.getYear());

        if(book.getChapterList() != null){
            List<Chapter> chapterList = new ArrayList<>();
            for (Chapter chapter : book.getChapterList()) {
                Chapter chapterData = new Chapter();
                chapterData.setId(chapter.getId());
                chapterData.setName(chapter.getName());
                chapterData.setContent(chapter.getContent());

                Book chapterBook = new Book();
                chapterBook.setId(chapter.getBook().getId());
                chapterBook.setTitle(chapter.getBook().getTitle());
                chapterBook.setAuthor(chapter.getBook().getAuthor());

                chapterData.setBook(chapterBook);
                chapterList.add(chapterData);
                bookDto.setChapterList(chapterList);
            }
        }

        return bookDto;
    }
}
