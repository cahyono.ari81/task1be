package com.example.OneToManyRelationDBTask2.controller;

import com.example.OneToManyRelationDBTask2.dto.ChapterDto;
import com.example.OneToManyRelationDBTask2.service.ChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/chapter")
public class ChapterController {

    @Autowired
    private ChapterService chapterService;

    @GetMapping("/name")
    public List<ChapterDto> joinBookByName(@RequestParam String name){
        return this.chapterService.getAllJoinBookByName(name);
    }
}
