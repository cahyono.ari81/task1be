package com.example.OneToManyRelationDBTask2.service;

import com.example.OneToManyRelationDBTask2.dto.BookDto;

import java.util.List;

public interface BookService {

    public List<BookDto> all();
    public BookDto create(BookDto bookDto);
}
