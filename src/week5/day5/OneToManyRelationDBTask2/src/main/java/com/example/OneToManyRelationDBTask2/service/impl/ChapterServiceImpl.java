package com.example.OneToManyRelationDBTask2.service.impl;

import com.example.OneToManyRelationDBTask2.dto.ChapterDto;
import com.example.OneToManyRelationDBTask2.model.Book;
import com.example.OneToManyRelationDBTask2.model.Chapter;
import com.example.OneToManyRelationDBTask2.repository.ChapterRepository;
import com.example.OneToManyRelationDBTask2.service.ChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ChapterServiceImpl implements ChapterService {

    @Autowired
    private ChapterRepository chapterRepository;

    @Override
    public List<ChapterDto> getAllJoinBookByName(String name) {
        final List<Chapter> chapters = this.chapterRepository.getAllJoinBookByName(name);
        List<ChapterDto> chapterDtos = new ArrayList<>();
        for (Chapter chapter : chapters) {
            chapterDtos.add(converToChapterDto(chapter));
        }
        return chapterDtos;
    }

    public ChapterDto converToChapterDto(Chapter chapter) {

        Book book = new Book();
        book.setId(chapter.getBook().getId());
        book.setTitle(chapter.getBook().getTitle());
        book.setAuthor(chapter.getBook().getAuthor());

        ChapterDto chapterDto = new ChapterDto();
        chapterDto.setId(chapter.getId());
        chapterDto.setName(chapter.getName());
        chapterDto.setContent(chapter.getContent());
        chapterDto.setIsDeleted(chapter.getIsDeleted());
        chapterDto.setBook(book);
        return chapterDto;
    }
}
