package com.example.OneToManyRelationDBTask1.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/welcome")
public class GettingStarted {

    @GetMapping
    public String welcome(){
        return "Welcome to my world";
    }
}
