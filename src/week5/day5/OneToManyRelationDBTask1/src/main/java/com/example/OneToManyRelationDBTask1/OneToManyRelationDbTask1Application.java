package com.example.OneToManyRelationDBTask1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OneToManyRelationDbTask1Application {

	public static void main(String[] args) {
		SpringApplication.run(OneToManyRelationDbTask1Application.class, args);
	}

}
