package com.example.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/start-test")
public class StartTest {

    @GetMapping("/")
    public ResponseEntity<String> welcome(){
        return new ResponseEntity<>("Welcome Ari", HttpStatus.OK);
    }

}
