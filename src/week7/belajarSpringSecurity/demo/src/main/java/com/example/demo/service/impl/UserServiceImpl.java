package com.example.demo.service.impl;

import com.example.demo.dto.UserDto;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

// Service adalah business layer
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDto store(User user) {
        User userStore = this.userRepository.save(user);
        return converToUserDto(user);
    }

    @Override
    public List<UserDto> index() {

        List<User> userList = this.userRepository.findAll();

        List<UserDto> userDtoList = new ArrayList<>();
        for (User user : userList){
            UserDto userDto = new UserDto();
            userDtoList.add(converToUserDto(user));
        }
        return userDtoList;
    }

    private UserDto converToUserDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getPassword());
        userDto.setAge(user.getAge());
        return userDto;
    }
}
