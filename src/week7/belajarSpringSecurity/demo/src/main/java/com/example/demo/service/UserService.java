package com.example.demo.service;

import com.example.demo.dto.UserDto;
import com.example.demo.model.User;

import java.util.List;

public interface UserService {
    UserDto store(User user);
    List<UserDto> index();
}
