package com.example.demo.dto;

import lombok.*;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class UserDto {
    private UUID id;
    private String username;
    private String password;
    private int age;
}
