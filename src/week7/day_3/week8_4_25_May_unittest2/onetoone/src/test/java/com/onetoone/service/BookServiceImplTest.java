package com.onetoone.service;

import com.onetoone.dto.BookChapterGroupResponse;
import com.onetoone.dto.BookDto;
import com.onetoone.repository.BookRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class BookServiceImplTest extends Constants {

    @Mock
    private BookRepository bookRepository;

    @InjectMocks
    private BookServiceImpl bookServiceImpl;

    @Test
    public void insertTest() {
        Mockito.when(bookRepository.save(Mockito.any())).thenReturn(getBookSaved());

        BookDto bookDto = bookServiceImpl.insert(getBookDto());

        Assertions.assertEquals(getBookSaved().getAuthor(), bookDto.getAuthor());
        Assertions.assertEquals(getBookSaved().getId(), bookDto.getId());
        Assertions.assertEquals(getBookSaved().getTitle(), bookDto.getTitle());
    }

    @Test
    public void getTotalBooks() {
        Mockito.when(bookRepository.getTotalBooks()).thenReturn(0);

        Integer totalBooks = bookServiceImpl.getTotalBooks();

        Assertions.assertEquals(totalBooks, 0);
    }

    @Test
    public void getBookChapterGroup() {
        Mockito.when(bookRepository.getBookChapter()).thenReturn(getBookChapterDto());

        List<BookChapterGroupResponse> bookChapterGroupResponses = bookServiceImpl.getBookChapterGroup();

        Assertions.assertEquals(bookChapterGroupResponses.size(), getBookChapterDto().size());
    }



}
