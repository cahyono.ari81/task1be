package com.onetoone.controller;

import com.onetoone.dto.BookChapterGroupResponse;
import com.onetoone.dto.BookDto;
import com.onetoone.service.BookServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class BookControllerTest extends Constants {
    @Mock
    private BookServiceImpl bookService;

    @InjectMocks
    private BookController bookController;

    @Test
    public void insertTest() {
        Mockito.when(this.bookService.insert(Mockito.any())).thenReturn(getBookSavedDto());

        ResponseEntity<BookDto> bookSavedDto = bookController.insert(getBookDto());

        Assertions.assertEquals(bookSavedDto.getBody().getTitle(), getBookSavedDto().getTitle());
        Assertions.assertEquals(bookSavedDto.getBody().getAuthor(), getBookSavedDto().getAuthor());
        Assertions.assertEquals(bookSavedDto.getBody().getId(), getBookSavedDto().getId());
        Assertions.assertEquals(bookSavedDto.getBody().getYear(), getBookSavedDto().getYear());
        Assertions.assertEquals(bookSavedDto.getBody().getChapterDtoList().size(), getBookSavedDto().getChapterDtoList().size());
    }

    @Test
    public void getBookChapterGroup(){
        Mockito.when(this.bookService.getBookChapterGroup()).thenReturn(getBookChapterGroupDto());
        ResponseEntity<List<BookChapterGroupResponse>> bookChapterGroup = this.bookController.getBookChapterGroup();
        Assertions.assertEquals(bookChapterGroup.getBody().size(), getBookChapterGroupDto().size());
    }

    @Test
    public void getTotalBooks(){
        Mockito.when(this.bookService.getTotalBooks()).thenReturn(10);
        ResponseEntity<Integer> totalBooks = this.bookController.getTotalBooks();
        Assertions.assertEquals(totalBooks.getBody().intValue(), 10L);
    }
}
