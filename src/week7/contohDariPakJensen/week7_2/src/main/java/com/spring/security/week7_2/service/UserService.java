package com.spring.security.week7_2.service;

import com.spring.security.week7_2.entiity.User;

import java.util.List;

public interface UserService {

    public List<User> getAll();

    public User create(User user);
}
