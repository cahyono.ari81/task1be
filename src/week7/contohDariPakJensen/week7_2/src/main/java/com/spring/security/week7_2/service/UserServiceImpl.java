package com.spring.security.week7_2.service;

import com.spring.security.week7_2.entiity.User;
import com.spring.security.week7_2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// service adalah business layer
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User create(User user) {
        return this.userRepository.save(user);
    }
}
