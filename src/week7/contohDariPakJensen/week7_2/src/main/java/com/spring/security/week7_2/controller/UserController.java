package com.spring.security.week7_2.controller;

import com.spring.security.week7_2.entiity.User;
import com.spring.security.week7_2.service.UserService;
import com.spring.security.week7_2.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/create")
    public ResponseEntity<User> create(@RequestBody User user) {
        User newUser = this.userService.create(user);

        return new ResponseEntity<>(newUser, HttpStatus.CREATED);
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<User>> getAll() {
        List<User> userList = this.userService.getAll();

        return new ResponseEntity<>(userList, HttpStatus.OK);
    }
}
