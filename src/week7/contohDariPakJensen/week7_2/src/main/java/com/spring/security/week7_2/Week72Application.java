package com.spring.security.week7_2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Week72Application {

	public static void main(String[] args) {
		SpringApplication.run(Week72Application.class, args);
	}

}
