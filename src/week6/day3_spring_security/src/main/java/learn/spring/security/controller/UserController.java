package learn.spring.security.controller;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/user")
public class UserController {
//
    @GetMapping("/welcome")
    public ResponseEntity<String> user(){
        return new ResponseEntity<>("welcome Ari", HttpStatus.OK );
    }

    @GetMapping("/generate-user")
    private String generateUser() {
        return "this.userService.generateUser()";
    }
}
