package learn.spring.security.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class Configuration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // super.configure(auth);
          auth.inMemoryAuthentication()
                .withUser("admin")
                .password("admin123")
                .roles("admin")
                .and()
                .withUser("customer")
                .password("customer123")
                .roles("customer");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // super.configure(http);
        http.authorizeRequests()
                .antMatchers("/api/user/welcome").hasRole("admin")
                .antMatchers("/api/user/getAllCustomer").hasAnyRole("customer", "admin")
                .antMatchers("/api/user/login").permitAll()
                .anyRequest().permitAll()
                .and().formLogin();

//        http.
//                httpBasic()
//                .and()
//                .authorizeRequests()
//                .antMatchers("/api/user/welcome").hasRole("admin")
//                .antMatchers("/details").hasAnyRole("admin_role","student")
//                .and()
//                .formLogin();
    }
    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }
}
