package week2.day1;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        // Get input by keyboard
        Scanner kb = new Scanner(System.in);
        System.out.print("Masukkan Kalimat : ");
        String sentence = kb.nextLine();

        // String backwards
        int lenghtSentence = sentence.length() - 1;
        System.out.print("Kalimat Tebalik = ");
        for (int i = lenghtSentence; i >= 0; i--) {
            System.out.print(sentence.charAt(i));
        }
    }
}

