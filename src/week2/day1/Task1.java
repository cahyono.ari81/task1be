package week2.day1;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {

        // Get input by keyboard
        Scanner kb = new Scanner(System.in);
        System.out.print("variable = ");
        String sentence = kb.nextLine();

        // Create Array
        char alfabet[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
                'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        int totalAlfabet[] = new int[26];

        // Check Alfabet
        int sentenceLength = sentence.length();
        int alfabetLength = alfabet.length;
        for (int i = 0; i < sentenceLength; i++) {
            for (int j = 0; j < alfabetLength; j++) {
                if (sentence.toLowerCase().charAt(i) == alfabet[j]) {
                    totalAlfabet[j]++;
                }
            }
        }

        // Print Result Alfabet
        int totalAlfabetLength = totalAlfabet.length;
        for (int i = 0; i < totalAlfabetLength; i++) {
            int resultAlfabet = totalAlfabet[i];
            if (resultAlfabet > 0) {
                System.out.println("alfabet " + alfabet[i] + " muncul " + resultAlfabet + " kali");
            }
        }

    }
}

