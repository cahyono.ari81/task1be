package week2.day4.pr1;

public abstract class Animal {
    protected int legs;
    protected String food;
    public Animal(int legs, String food){
        this.legs = legs;
        this.food = food;
    }
    public abstract int getNumberOfLegs();
    public abstract String getFavoriteFood();
}
