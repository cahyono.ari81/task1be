package week2.day4.pr1;

public class Fly extends Animal{
    public Fly(int legs, String food) {
        super(legs, food);
    }

    @Override
    public int getNumberOfLegs() {
        return super.legs;
    }

    @Override
    public String getFavoriteFood() {
        return super.food;
    }
}
