package week2.day4;

import week2.day4.pr2.Account;
import week2.day4.pr2.SavingsAccount;

public class Pr2 {

    public static void main(String[] args) {
        Account savingsAccount = new Account("Ari Cahyono", "03425");
        SavingsAccount savingsAccount2 = new SavingsAccount("Ari Cahyono", "03425", 250.45);

        System.out.println("Hasil Dari toString() untuk Account Adalah : ");
        System.out.println(savingsAccount + "\n");
        System.out.println("Hasil Dari toString() untuk SavingsAccount Adalah : ");
        System.out.println(savingsAccount2);
    }


}
