package week2.day4;

import week2.day4.pr1.*;

public class Pr1 {
    public static void main(String[] args) {
        Animal[] hewans = new Animal[6];

        hewans[0] = new Dog(4, "Daging");
        hewans[1] = new Spider(8, "Serangga");
        hewans[2] = new Fly(0, "Kotoran");
        hewans[3] = new Centipede(100, "Daun");
        hewans[4] = new Snake(0, "Tikus");
        hewans[5] = new Chicken(2, "Kacang");

        // Addition information
        String[] addInfoHewan = {"Dog", "Serangga", "Fly", "Centipede", "Snake", "Chicken"};

        int hewansLength = hewans.length;
        for (int i = 0; i < hewansLength; i++) {
            System.out.println("Animal : " + addInfoHewan[i]);
            System.out.println("Total legs : " + hewans[i].getNumberOfLegs());
            System.out.println("Food : " + hewans[i].getFavoriteFood());
            System.out.println("============================");
        }
    }
}
