package week2.day4.pr2;

public class SavingsAccount extends Account {

    protected double balance;

    public SavingsAccount(String name, String accountNumber, double balance) {
        super(name, accountNumber);
        this.balance = balance;
    }

    void deposit(double value){

    }

    void withDraw(double value){

    }

    @Override
    public String toString() {
        return this.accountNumber + "\n" + this.name + "\n" + "$"+this.balance;
    }
}
