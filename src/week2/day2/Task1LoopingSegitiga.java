package week2.day2;

public class Task1LoopingSegitiga {
    public static void main(String[] args) {
        //Looping Segitiga @
        int initValue = 10;
        for (int i = 0; i < initValue; i++) {
            for (int j = initValue; j >= 0; j--) {
                String result = j < i ? "@" : " ";
                System.out.print(result);
            }
            System.out.println("");
        }
    }
}
